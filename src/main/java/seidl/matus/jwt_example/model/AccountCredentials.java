package seidl.matus.jwt_example.model;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-10
 */
public class AccountCredentials {
    private String username;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
